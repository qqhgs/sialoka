(function (window) {
  var domReadyQueue = [],
    handleDOMReady = function (fn) {
      return document.readyState === "complete"
        ? fn.call(document)
        : domReadyQueue.push(fn);
    };

  document.addEventListener("DOMContentLoaded", function onDOMReady() {
    document.removeEventListener("DOMContentLoaded", onDOMReady);
    while (domReadyQueue.length) {
      domReadyQueue.shift().call(document);
    }
  });

  function Qiu(selector) {
    this.nodes = [];

    if (!(this instanceof Qiu)) {
      return new Qiu(selector);
    }

    if (typeof selector === "function") {
      return handleDOMReady(selector);
    }

    if (selector instanceof HTMLElement || selector instanceof NodeList) {
      this.nodes = selector.length > 1 ? [].slice(selector) : [selector];
    } else if (typeof selector === "string") {
      if (selector[0] === "<" && selector[selector.length - 1] === ">") {
        this.nodes = [createNode(selector)];
      } else {
        switch (selector) {
          case "window":
            this.nodes = [window];
            break;
          case "document":
            this.nodes = [document];
            break;
          default:
            this.nodes = [].slice.call(document.querySelectorAll(selector));
        }
      }
    }

    // Store all result of query to 'this'
    if (this.nodes.length) {
      this.length = this.nodes.length;
      for (var i = 0; i < this.nodes.length; i++) {
        this[i] = this.nodes[i];
      }
    }

    // Create the element dynamically
    function createNode(html) {
      var div = document.createElement("div");
      div.innerHTML = html;
      return div.firstChild;
    }

    // Create fn shorthand
    Q.fn = Qiu.prototype;

    // Looping
    Q.fn.each = function (callback) {
      for (var i = 0; i < this.length; i++) {
        callback.call(this, this[i], i);
      }
      return this;
    };

    // Get getAttribute & setAttribut
    Q.fn.attr = function (name, value) {
      if (!this[0]) return;
      if (!value) {
        return this[0].getAttribute(name);
      }
      return this.each(function (e) {
        e.setAttribute(name, value);
      });
    };

    // Checking if element has Class
    Q.fn.hasClass = function (className) {
      var array = [];
      this.each(function (e) {
        var classList = Q(e).attr("class");
        if (classList === null || classList === "") {
          array.push(false);
        } else {
          classList.split(" ").indexOf(className) !== -1
            ? array.push(true)
            : array.push(false);
        }
      });
      return array.indexOf(false) !== -1 ? false : true;
    };

    // Add class to element
    Q.fn.addClass = function (classname) {
      this.each(function (e) {
        var classList = Q(e).attr('class');
        if ( classList === null || classList === '' ) {
          Q(e).attr('class', classname)
        } else {
          var array = classList.split(' ').concat(classname.split(' '));
          Q(e).attr('class', array.filter(function (e, i, a) {
            return a.indexOf(e) === i
          }).join(' '))
        }
      });
      return this;
    };

    // Remove class from element
    Q.fn.removeClass = function (className) {
      this.each(function (e) {
        var classList = Q(e).attr('class');
        if ( classList === null ) return;
        if ( classList === className ) {
          e.removeAttribute('class')
        } else {
          classList = classList.split(' ');
          for (var i = 0; i < className.split(' ').length; i++) {
            classList.forEach(function(e, j, a){
              if ( e === className.split(' ')[i] ) {
                a.splice(j, 1)
              }
            })
          }
          Q(e).attr('class', classList.join(' '))
        }
      });
      return this;
    };

    // Toggle class on element
    Q.fn.toggleClass = function (className, onAdd, onRemove) {
      this.each(function (e) {
        if ( Q(e).hasClass(className) ) {
          Q(e).removeClass(className)
          if (typeof onRemove === "function") onRemove();
        } else {
          Q(e).addClass(className)
          if (typeof onAdd === "function") onAdd();
        }
      })
      return this
    }

    // Add event listener
    Q.fn.on = function (event, handler) {
      this.each(function (e) {
        e.addEventListener(event, handler);
      });
    };

    // Check if element has element
    Q.fn.has = function (el) {
      var value;
      this.each(function (e) {
        value = e.outerHTML.indexOf(el.outerHTML) !== -1 ? true : false;
      });
      return value;
    };

    // Get inner HTML or put something to inner HTML
    Q.fn.html = function (val) {
      if (val === undefined) {
        return this[0].innerHTML;
      }
      this.each(function (e) {
        e.innerHTML = val;
      });
      return this; // to support method chaining
    };

  } // End of constructor

  // Initiate
  var init = function (e) {
    return new Qiu(e);
  };
  window.Q = init;
})(window);

Q.ajax = function (url, callback, method) {
  if (method === undefined) method = "GET";
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4) {
      if (xhr.status == 200) {
        var res = JSON.parse(xhr.responseText);
        callback(res);
      }
    }
  };
  xhr.open(method, url);
  xhr.send();
};
