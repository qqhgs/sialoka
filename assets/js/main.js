// Save scroll states
Q('window').on('scroll', function(){
  document.documentElement.style.setProperty('--scroll-y', Math.round(window.pageYOffset) + 'px')
})

// All click event
Q(".toggle-btn-slide-menu").on("click", function (ev) {
  Q("html").toggleClass("slide-menu-opened", function() {
    var scrollY = document.documentElement.style.getPropertyValue('--scroll-y')
    document.body.style.top = '-' + scrollY;
    if (Q(".mobile-menus").html().trim() === "") {
      var html = "<ul>";
      Q('nav li a').each(function(e){
        html += Q('<li>').attr('class', 'item').html(
          Q('<a>').attr('href', 
            Q(e).attr('href')
          ).html(
            Q(e).html()
          )[0].outerHTML
        )[0].outerHTML
      })
      Q('.mobile-menus').html(html)
    }
  }, function() {
    window.scrollTo(0, parseInt(document.body.style.top || '0') * -1)
    document.body.style.top = '';
  })
});

Q(".toggle-btn-slide-search").on("click", function () {
  Q("html").toggleClass("open-search-section", function() {
    var scrollY = document.documentElement.style.getPropertyValue('--scroll-y');
    document.body.style.top = '-' + scrollY;
  }, function() {
    window.scrollTo(0, parseInt(document.body.style.top || '0') * -1)
    document.body.style.top = '';
  })
});

// Search Feature
var searchStates = function (searchText) {
  Q.ajax("/sialoka/data/post/", function (states) {
      var matches = [];

      states.forEach(function (state) {
        var regex = new RegExp(searchText, "gi");
        if (state.title.match(regex)) {
          matches.push(state);
        }
      });

      if (searchText.length === 0) {
        matches = [];
        Q("#search-section .results").html("");
      }

      outputHtml(matches);
    }
  );
};

function outputHtml(matches) {
  if (matches.length > 0) {
    var html = "";
    matches.forEach(function (match, index) {
      if (index < 4) {
        html +=
          '<li class="item"><a href="' +
          match.url +
          '">' +
          match.title +
          "</a></li>";
      }
    });

    Q("#search-section .results").html('<ul class="items">' + html + "</ul>");
  }
}

Q("#search-section .search-form").on("input", function () {
  searchStates(this.value);
});

// Auto-hide navbar
var prevPos = window.pageYOffset;
Q('window').on('scroll', function(){
  var currentPos = window.pageYOffset;
  if (prevPos < currentPos && currentPos > '100') {
      Q('html').addClass('navbar-hide')
  } else {
      Q('html').removeClass('navbar-hide')
  }
  prevPos = currentPos;
})
