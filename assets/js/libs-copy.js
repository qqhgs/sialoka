(function (window, undefined) {
  var domReady$ueue = [],
    handleDOMReady = function (fn) {
      return document.readyState === "complete"
        ? fn.call(document)
        : domReady$ueue.push(fn);
    };

  document.addEventListener("DOMContentLoaded", function onDOMReady() {
    document.removeEventListener("DOMContentLoaded", onDOMReady);
    while (domReady$ueue.length) {
      domReady$ueue.shift().call(document);
    }
  });

  function jQueue(selector) {
    this.nodes = [];

    if (!(this instanceof jQueue)) {
      return new jQueue(selector);
    }

    if (typeof selector === "function") {
      return handleDOMReady(selector);
    }

    if (selector instanceof HTMLElement || selector instanceof NodeList) {
      this.nodes = selector.length > 1 ? [].slice(selector) : [selector];
    } else if (typeof selector === "string") {
      if (selector[0] === "<" && selector[selector.length - 1] === ">") {
        this.nodes = [createNode(selector)];
      } else {
        switch (selector) {
          case 'window':
            this.nodes = [window]
            break;
          case 'document':
            this.nodes = [document]
            break;
          case 'body':
            this.nodes = [document.body]
            break;
          default:
            this.nodes = [].slice.call(document.querySelectorAll(selector))
        }
      }
    }

    // Store all result of query to 'this'
    if (this.nodes.length) {
      this.length = this.nodes.length;
      for (var i = 0; i < this.nodes.length; i++) {
        this[i] = this.nodes[i];
      }
    }

    // Create the element dynamically
    function createNode(html) {
      var div = document.createElement("div");
      div.innerHTML = html;
      return div.firstChild;
    }


    // $.fn =
    $.fn = jQueue.prototype

    $.fn.each = function (callback) {
        for(var i = 0; i < this.length; i++) {
          callback.call(this, this[i], i);
        }
        return this;
      }

    $.fn.attr = function ( name, value ) {
      if (!this[0]) return;
      if ( !value ) {
        return this[0].getAttribute(name);
      }
      return this.each(function(e){
        e.setAttribute( name, value );
      });
    }

    $.fn.hasClass = function (className) {
      var array = [];
      this.each(function(e){
        var classList = $(e).attr('class');
        if (classList === null || classList === '') {
          array.push(false)
        } else {
          classList.split(' ').indexOf(className) !== -1
            ? array.push(true)
            : array.push(false)
        }
      })
      return array.indexOf(false) !== -1 ? false : true
    }

    $.fn.addClass = function(className) {
      this.each(function(e){
        e.classList.add(className)
      })
      return this
    }
    
    $.fn.removeClass = function (className) {
      this.each(function(e){
        e.classList.remove(className)
      })
      return this;
    }

    $.fn.on = function (event, handler) {
      this.each(function(e) {
        e.addEventListener(event, handler);
      })
    }

    $.fn.has = function(el) {
      var value;
      this.each(function(e){
        value = e.outerHTML.indexOf(el.outerHTML) !== -1 ? true : false
      })
      return value
    }

    $.fn.html = function (val) {
      if (val === undefined) {
        return this[0] && this[0].innerHTML;
      }
      this.each(function(e) {
        e.innerHTML = val;
      });
      return this;   // to support method chaining
    }

    $.fn.css = function (prop, val) {
      var result = '';
      this.each(function(e){
        var style = $(e).attr('style');
        if (typeof prop === 'string') {
          if (val === undefined) {
            result = e.style.getPropertyValue(prop);
          } else {
            style === null || style === ''
              ? $(e).attr('style', prop + ': ' + val + ';')
              : $(e).attr('style', style + prop + ':' + val + ';')
            result = this
          }
        }
        if ( typeof prop === 'object') {
          var css = '';
          for ( var key in prop ) {
            css += key + ':' + prop[key] + ';'
          }
          $(e).attr('style', style + css)
          result = this
        }
      })
      return result
    }

    $.fn.ajax = function (obj) {
      if (obj.method === undefined) obj.method = "GET";
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
          if (xhr.status == 200) {
            var res = JSON.parse(xhr.responseText);
            obj.success(res);
          }
        }
      }
      xhr.open("GET", obj.url);
      xhr.send();
    }

  } // End of jQueue constructor

  window.$ = window.jQueue = jQueue;

})(window);
