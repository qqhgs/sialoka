Array.prototype.each = function (callback, thisArg) {
  var T, k, O = Object(this), len = O.length >>> 0;
  if (arguments.length > 1) T = thisArg;
  k = 0;
  while (k < len) {
    var kValue;
    if (k in O) {
      kValue = O[k];
      callback.call(T, kValue, k, O);
    }
    k++;
  }
};
